const express = require('express');
const {google} = require('googleapis');
const CronJob = require('cron').CronJob;
const Config = require('./config.json');
const osu = require('node-osu');

const app = express();
app.use(express.json());

app.listen(7272, async () => {
    console.log("<init> I am ready!");
    new CronJob('0 0 * * * *', callCron, null, true, 'Europe/Berlin');
    checkNames();
});

app.get('/', (_request, response) => {
    response.status(727).send("A phrase osu! players use whenever 727 is seen, anywhere it is seen, mostly excluding punctuations. A reference to Cookiezi's (former #1 osu! player) choke on Blue Zenith +HR (and later +HDHR) worth 727pp (performance points)\n" +
        "\n" +
        "Often abbreviated to WYSI.\n" +
        "\n" +
        "Variations include: When you fucking see it (WYFSI), When you don't see it (WYDSI, whenever it is 1 digit off from 727 such as 717 and 737), etc.\n" +
        "Example 1:\n" +
        "osu! Player: no I choked my first 7* fc... wait MY ACC IS 97.27% WHEN YOU SEE IT\n" +
        "Example 2:\n" +
        "osu! Player: *looks outside and sees a truck with the number 727*\n" +
        "osu! Player: WHEN YOU SEE IT WYSI\n" +
        "Example 3:\n" +
        "osu! Player 1: lol check the time\n" +
        "osu! Player 2: OMG WYSI WYSI IT'S 7:27 PM");
});

app.get('/refresh', async (_request, response) => {
    console.log("Refresh triggered via http request");
    await response.status(200).send("Successfully triggered an update to the player list!");
    await checkNames();
});

async function authorize() {
    const authentication = new google.auth.GoogleAuth({
        keyFile: 'credentials.json',
        scopes: 'https://www.googleapis.com/auth/spreadsheets'
    });

    const sheetsClient = await authentication.getClient();
    return google.sheets({
        version: 'v4',
        auth: sheetsClient
    });
}

function callCron() {
    console.log("Refresh triggered via cron job");
    checkNames();
}

async function checkNames() {
    const sheets = await authorize();
    const response = await sheets.spreadsheets.values.get({
        spreadsheetId: Config.sheetids.refsheet,
        range: Config.refsheet.range
    });
    const playerData = await convertSignupData(response.data.values);
    await sheets.spreadsheets.values.clear({
        spreadsheetId: Config.sheetids.mainsheet,
        range: Config.mainsheet.range
    });
    await sheets.spreadsheets.values.append({
        spreadsheetId: Config.sheetids.mainsheet,
        range: Config.mainsheet.range,
        valueInputOption: 'USER_ENTERED',
        resource: {
            values: playerData
        }
    });
    console.log("Finished updating!");
}

async function convertSignupData(dataset) {
    const players = [];
    const duplicateCheck = [];
    for (let i = 0; i < dataset.length; i++) {
        let userId;
        let osuLinkUserInput = dataset[i][Config.refsheet.indexes.profilelink];
        if (osuLinkUserInput.match(/osu.ppy.sh\/users\/\d{1,7}/) || osuLinkUserInput.match(/osu.ppy.sh\/u\/\d{1,7}/)) {
            let searchArray = osuLinkUserInput.split('/');
            let isUserIdNext = false;
            for (const element of searchArray) {
                if (isUserIdNext) {
                    userId = element;
                    break;
                } else if (element === "users" || element === "u") {
                    isUserIdNext = true;
                }
            }
        } else {
            userId = osuLinkUserInput;
        }

        const osuApi = new osu.Api(Config.osuApiToken, {});
        const osuUser = await osuApi.getUser({
            u: userId
        });
        if (!duplicateCheck.includes(osuUser.id)) {
            if (osuUser.pp.rank > 100000) {
                if (osuUser.pp.rank < 300000) {
                    players.push(
                        [`=HYPERLINK("https://osu.ppy.sh/users/${osuUser.id}","${osuUser.name}")`, dataset[i][Config.refsheet.indexes.discordtag], `#${osuUser.pp.rank}`, `'${dataset[i][Config.refsheet.indexes.timezone]}`, `=IMAGE("https://osuflags.omkserver.nl/${osuUser.country}.png")`]
                    );
                    console.log(`[${i + 1}/${dataset.length}] ${osuUser.name} processed`);
                } else {
                    console.log(`[${i + 1}/${dataset.length}] ${osuUser.name} removed because they are underranked (#${osuUser.pp.rank})`);
                }
            }else{
                console.log(`[${i + 1}/${dataset.length}] ${osuUser.name} removed because they are overranked (#${osuUser.pp.rank})`);
            }
            duplicateCheck.push(osuUser.id);
        } else {
            console.log(`[${i + 1}/${dataset.length}] Duplicate registration for User ${osuUser.name} found! Skipping duplicate...`);
        }
        await sleep(300);
    }
    return players;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
