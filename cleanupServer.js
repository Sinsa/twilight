const {Client, Intents} = require('discord.js');
const client = new Client({intents: [Intents.FLAGS.GUILDS]});
const Config = require('./config.json')

client.on('ready', async () => {
    console.log('<init> I am ready!');
    const twilightServer = await client.guilds.fetch('854878216162246656');
    const memberlist = await twilightServer.members.list({limit: 1000});
    let kickedCount = 0;
    for (const serverMember of memberlist) {
        if (!serverMember[1].roles.cache.has('854882352638984192') && serverMember[1].user.id !== client.user.id) {
            console.log(`Kicking ${serverMember[1].user.tag}...`);
            await twilightServer.members.kick(serverMember[1].user.id);
            kickedCount++;
        } else {
            console.log(`Not kicking ${serverMember[1].user.tag} because they are a staff member...`);
        }
        await sleep(500);
    }
    console.log(`Done! Kicked ${kickedCount} members.`);
    process.exit(727);
});

client.login(Config.discordtoken);


async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

